<div align="center">

# ACTIVIDAD 7 - Javascript - Rock Paper Scissors Lizard Spock

[![Estado de construcción](https://img.shields.io/static/v1?label=Estado%20de%20Construcción&message=Finalizado&color=brightgreen)](https://gitlab.com/gusgonza/mp04-actividad-7)

</div>


Este proyecto consiste en una página web que simula el juego RPSLS (Rock Paper Scissors Lizard Spock) contra la máquina.

## Descripción del Juego

RPSLS es una extensión del clásico juego de azar, Piedra Papel Tijeras, creado por Sam Kass y Karen Bryla. Sam Kass explicó que creó el juego expandido porque parecía que la mayoría de los juegos de Piedra Papel Tijeras con personas que conoces terminarían en empate. El juego RPSLS fue mencionado por primera vez en el episodio de la temporada 2, "La Expansión del Lagarto-Spock", cuyo título hace referencia al juego. Fue mencionado por última vez en "La Disolución de Rothman". Como Sheldon explica, "Tijeras corta papel, papel cubre piedra, piedra aplasta lagarto, lagarto envenena Spock, Spock destroza tijeras, tijeras decapita lagarto, lagarto come papel, papel refuta a Spock, Spock vaporiza piedra, y como siempre, piedra aplasta tijeras."

## Reglas del Juego

- Papel cubre piedra
- Piedra aplasta lagarto
- Lagarto envenena Spock
- Spock destroza tijeras
- Tijeras decapita lagarto
- Lagarto come papel
- Spock vaporiza piedra
- Papel refuta a Spock
- Y como siempre, piedra aplasta tijeras.

## Youtube Video

[Video explicativo del juego](https://www.youtube.com/watch?v=x5Q6-wMx-K8&t=1s)

## Lógica del Proyecto

- El usuario debe elegir una opción.
- Una vez elegida, se muestra la opción seleccionada.
- Al hacer clic en "JUGAR", la máquina elige aleatoriamente una opción.
- Se comparan las opciones y se indica quién ha ganado.
- Se actualiza el marcador.


## Ayuda

1. **setTimeout**: Para ejecutar una función con un tiempo de espera, por ejemplo 200 milisegundos:
    ```javascript
    setTimeout(function () {
        // TODO
    }, 200);
    ```
    Si se desea ejecutar una o más funciones después de un tiempo, por ejemplo 1 minuto:
    ```javascript
    setTimeout(function () {
        document.getElementById("results").innerHTML = results();
        getPoints();
    }, 1000);
    ```

2. **Modificar un atributo de un elemento**: Para modificar un atributo, por ejemplo el atributo "src" del elemento "img" con id "image_computer".
    ```javascript
    document.getElementById("image_computer").setAttribute("src", path + "rock.png");
    ```

3. **Random**: Para elegir un número aleatorio entre 1 y 5.
    ```javascript
    const ran = Math.floor(Math.random() * 5);
    ```

4. **addEventListener**: Para agregar un evento a un contenedor y ejecutar una función.
    ```javascript
    document.getElementById("rock").addEventListener("click", function () {
        // TODO
    });
    ```

## Contenido del Repositorio

El repositorio contiene la siguiente estructura de carpetas y archivos:

- **assets**: Carpeta que contiene los recursos necesarios para la página web.
  - **css/**: Carpeta que contiene los archivos CSS.
  - **img/**: Carpeta que contiene las imágenes utilizadas en la página.
  - **js/**: Carpeta que contiene los archivos JavaScript.
- **index.html**: Archivo HTML que contiene la estructura de la página web.
- **Actividad-7.pdf**: Documento PDF que describe los requisitos de la actividad.
- **README.md**: Archivo README del proyecto.
