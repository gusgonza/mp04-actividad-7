// Ruta de la carpeta de imágenes
const path = "./assets/img/";

// Función para iniciar el juego
function play() {
	// Obtener la elección del usuario
	const userChoiceSrc = document.getElementById("user-choice").getAttribute("src");

	// Selección aleatoria de la computadora
	const randomNumber = Math.floor(Math.random() * 5) + 1;
	let computerChoiceSrc;
	switch (randomNumber) {
		case 1:
			computerChoiceSrc = path + "rock.png";
			break;
		case 2:
			computerChoiceSrc = path + "paper.png";
			break;
		case 3:
			computerChoiceSrc = path + "scissors.png";
			break;
		case 4:
			computerChoiceSrc = path + "lizard.png";
			break;
		case 5:
			computerChoiceSrc = path + "spock.png";
			break;
	}
	document.getElementById("computer-choice").setAttribute("src", computerChoiceSrc);
	document.getElementById("computer-choice").style.visibility = "visible";

	// Llamar a la función para determinar el resultado después de un retardo de 1000 ms (1 segundo)
	setTimeout(function () {
		results(userChoiceSrc, computerChoiceSrc);
	}, 500);
}

// Obtener todas las imágenes de elección del usuario
const images = document.querySelectorAll(".user-choice");
// Agregar evento de clic a cada imagen
images.forEach(image => {
	image.addEventListener("click", function () {
		// Obtener la ruta de la imagen seleccionada por el usuario
		const userChoiceSrc = this.getAttribute("src");
		// Establecer la elección del usuario en la imagen correspondiente
		document.getElementById("user-choice").setAttribute("src", userChoiceSrc);
	});
});

// Función para determinar los resultados del juego
function results() {
	// Obtener la elección del usuario y de la computadora
	const userChoiceSrc = document.getElementById("user-choice").getAttribute("src");
	const computerChoiceSrc = document.getElementById("computer-choice").getAttribute("src");

	if (userChoiceSrc === computerChoiceSrc) {
		// Si las elecciones son iguales, es un empate
		document.getElementById("result-message").innerText = "Empate";
	} else {
		// Obtener las opciones elegidas sin la ruta de la imagen
		const userChoice = userChoiceSrc.substring(userChoiceSrc.lastIndexOf("/") + 1, userChoiceSrc.lastIndexOf("."));
		const computerChoice = computerChoiceSrc.substring(computerChoiceSrc.lastIndexOf("/") + 1, computerChoiceSrc.lastIndexOf("."));

		// Lógica para determinar el resultado del juego
		if (
			(userChoice === "rock" && (computerChoice === "scissors" || computerChoice === "lizard")) ||
			(userChoice === "paper" && (computerChoice === "rock" || computerChoice === "spock")) ||
			(userChoice === "scissors" && (computerChoice === "paper" || computerChoice === "lizard")) ||
			(userChoice === "lizard" && (computerChoice === "spock" || computerChoice === "paper")) ||
			(userChoice === "spock" && (computerChoice === "scissors" || computerChoice === "rock"))
		) {
			// Si el usuario gana, mostrar el mensaje y aumentar la puntuación del usuario
			document.getElementById("result-message").innerText = "Ganaste";
			incrementUserScore();
		} else {
			// Si la computadora gana, mostrar el mensaje y aumentar la puntuación de la computadora
			document.getElementById("result-message").innerText = "La máquina ganó";
			incrementComputerScore();
		}
	}
}

// Puntuaciones del usuario y de la computadora
let userScore = 0;
let computerScore = 0;

// Función para incrementar la puntuación del usuario
function incrementUserScore() {
	userScore++;
	document.getElementById("user-score").innerText = userScore;
}

// Función para incrementar la puntuación de la computadora
function incrementComputerScore() {
	computerScore++;
	document.getElementById("computer-score").innerText = computerScore;
}
